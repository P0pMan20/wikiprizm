---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018=BasicToken:List1\\~List6= == Description\n==\
    \ This recalls Lists. You can store into them as well. == Syntax\n==\u2019\u2018\
    \u2019List 1\\~6\u2019\u2019\u2019 \u2018\u2019Lists\u2019\u2018\u2192\u2019\u2018\
    \u2019List 1\\~6\u2019\u2019\u2019 == Example == List 1 \u2026\u2019"
  timestamp: '2012-02-29T20:04:44Z'
title: List1~List6
---

# BasicToken:List1\~List6

## Description

This recalls Lists. You can store into them as well.

## Syntax

**List 1\~6**

*Lists*→**List 1\~6**

## Example

    List 1
    {1,2,3,4,5}→List 6
