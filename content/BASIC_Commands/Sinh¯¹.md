---
revisions:
- author: Turiqwalrus
  comment: "Created page with \u2018= Sinh\xAF\xB9 = == Description == This command\n\
    returns the hyperbolic arcsine of the value. == Syntax\n==\u2019\u2018\u2019Sinh\xAF\
    \xB9\u2019\u2019\u2019 \u2018\u2019Value\u2019\u2019 == Example == Sinh\xAF\xB9\
    \ .75\u2019"
  timestamp: '2012-02-29T20:27:12Z'
title: "Sinh\xAF\xB9"
---

# Sinh¯¹

## Description

This command returns the hyperbolic arcsine of the value.

## Syntax

**Sinh¯¹** *Value*

## Example

`Sinh¯¹ .75`
