---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Xscl = == Description == This command\nsets\
    \ the scale of x displayed in the graphscreen. == Syntax\n==\u2019\u2018Value\u2019\
    \u2018\u2192Xscl == Example == 3\u2192\u2019\u2018\u2019Xscl\u2019\u2019\u2019\
    \n\\[\\[Category:BASIC_Commands\u2026\u2019"
  timestamp: '2012-02-24T20:32:24Z'
title: Xscl
---

# Xscl

## Description

This command sets the scale of x displayed in the graphscreen.

## Syntax

*Value*→Xscl

## Example

`3→`**`Xscl`**
