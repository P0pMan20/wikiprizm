---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= tan\xAF\xB9 = == Description == This command\n\
    returns the arctangent of the value. == Syntax ==\u2019\u2018\u2019tan\xAF\xB9\
    \u2019\u2019\u2019\n\u2018\u2019Value\u2019\u2019 == Example == tan\xAF\xB9 .75\u2019"
  timestamp: '2012-02-29T20:22:38Z'
title: "Tan\xAF\xB9"
---

# tan¯¹

## Description

This command returns the arctangent of the value.

## Syntax

**tan¯¹** *Value*

## Example

`tan¯¹ .75`
