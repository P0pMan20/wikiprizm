---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Then = == Description == This command is\nused\
    \ with If-Then statement along with\u2019\u2018\u2019IfEnd\u2019\u2019\u2018,\
    \ and\u2019\u2018\u2019If\u2019\u2019\u2018. ==\nSyntax ==\u2019\u2018\u2019If\u2019\
    \u2019\u2019 \u2018\u2019Condition\u2019\u2019 \u2019\u2018\u2019Then\u2019\u2019\
    \u2019 \u2026Codes\u2026 \u2019\u2018\u2019IfE\u2026\u2019"
  timestamp: '2012-02-16T23:12:45Z'
title: Then
---

# Then

## Description

This command is used with If-Then statement along with **IfEnd**, and
**If**.

## Syntax

**If** *Condition*

**Then**

...Codes...

**IfEnd**

## Example

    If A=6
    Then
    A+3
    IfEnd
