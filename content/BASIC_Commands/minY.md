---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= minY = == Description == This is for\nspecifying\
    \ minimum y value in y axis at graphscreen. == Syntax\n==\u2019\u2018Value\u2019\
    \u2018\u2192\u2019\u2018\u2019MinY\u2019\u2019\u2019 == Example == 3\u2192MinY\u2019"
  timestamp: '2012-02-17T01:01:37Z'
title: minY
---

# minY

## Description

This is for specifying minimum y value in y axis at graphscreen.

## Syntax

*Value*→**MinY**

## Example

`3→MinY`
