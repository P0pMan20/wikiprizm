---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= a(Reg) = == Description == This is\nthe\u2019\
    \u2018\u2019a\u2019\u2019\u2019 value used in regression. Can be treated as a\
    \ number.\nRegression including \u2019\u2018\u2019a\u2019\u2019\u2019 must be\
    \ calculated before usage. ==\nSyntax ==\u2026\u2019"
  timestamp: '2012-02-15T19:59:17Z'
title: a(Reg)
---

# a(Reg)

## Description

This is the **a** value used in regression. Can be treated as a number.
Regression including **a** must be calculated before usage.

## Syntax

**a**

## Example

`a`

`a+4`
