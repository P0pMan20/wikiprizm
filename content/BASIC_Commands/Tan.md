---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= tan = == Description == This command\nreturns\
    \ the tangent of the value. == Syntax ==\u2019\u2018\u2019tan\u2019\u2019\u2019\
    \ \u2018\u2019value\u2019\u2019 ==\nExample == tan 30 \\[\\[Category:BASIC_Commands\\\
    ]\\]\u2019"
  timestamp: '2012-02-15T03:47:36Z'
title: Tan
---

# tan

## Description

This command returns the tangent of the value.

## Syntax

**tan** *value*

## Example

`tan 30`
