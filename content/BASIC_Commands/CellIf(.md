---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= CellIf( = == Description == This command\nreturns\
    \ expression 1 when the equality or inequality provided as the\nbranch condition\
    \ is true, and expression 2 when it is false. == Sy\u2026\u2019"
  timestamp: '2012-02-15T23:55:24Z'
title: CellIf(
---

# CellIf(

## Description

This command returns expression 1 when the equality or inequality
provided as the branch condition is true, and expression 2 when it is
false.

## Syntax

**CellIf(***equality*,*expression 1*,*expression 2***)**

or

**CellIf(***inequality*,*expression 1*,*expression 2***)**

## Example

`CellIf(A1>B1,A1,B1)`
