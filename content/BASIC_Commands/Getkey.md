---
revisions:
- author: Purobaz
  timestamp: '2012-02-22T22:33:40Z'
title: Getkey
---

# Getkey

## Description

This command returns the code that corresponds to the only last key
pressed. If any key is pressed, the command returns a value of zero.\
This command doesn't wait, so it can be use inside a loop.

## Syntax

**Getkey**\
Returns the value in Ans.

**Getkey-\>G**\
Returne the value in G.

## Key codes {#key_codes}

This is [key map](http://prizm.cemetech.net/images/prizm_key_code.png)

## Examples

Getkey wait :

    Do
    Getkey->G
    LpWhile Not G

Getkey release :

    While Getkey
    WhileEnd

Numbers (0\~9) :

    ClearText
    Do
    'Getkey release
    While Getkey
    WhileEnd
    'Getkey wait
    Do
    Getkey->G
    LpWhile Not G
    .1G
    2-Ans+31Frac Ans+2(G=71
    Ans>=0=>Ans<=9=>Int .1G>4Locate 1,1,Ans
    LpWhile G!=47 And G!=48

Letters (1\~26) :

    ClearText
    Do
    'Getkey release
    While Getkey
    WhileEnd
    'Getkey wait
    Do
    Getkey->G
    LpWhile Not G
    .1G
    6-10Frac Ans->R
    8-Int Ans+5R+R(R<3
    Ans>=1=>Ans<=26=>Locate 1,1,Ans
    LpWhile G!=47 And G!=48
