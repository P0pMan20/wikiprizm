---
revisions:
- author: Turiqwalrus
  timestamp: '2012-03-01T20:00:49Z'
title: StrLeft(
---

## StrLeft(

## Description

This command reads a number of letters from the string's beginning(the
left part of it)

## Syntax

`StrLeft(<String name>,<number of letters to read>)`

## Example

    "CASIO PRIZM" → Str 1
    Locate 1,1,StrLeft(Str 1,5)                    //Displays the first 5 characters of the string(CASIO)
