---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-24T20:29:57Z'
title: Xmax
---

# XMax

## Description

This command sets the maximum x value displayed in the graphscreen.

## Syntax

*Value*→XMax

## Example

`3→`**`Xmax`**
