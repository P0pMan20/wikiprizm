---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= ClrGraph = == Description == This\ncommand clears\
    \ the graphscreen. == Syntax ==\u2019\u2018\u2019ClrGraph\u2019\u2019\u2019 ==\n\
    Example == ClrGraph \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-16T00:48:13Z'
title: ClrGraph
---

# ClrGraph

## Description

This command clears the graphscreen.

## Syntax

**ClrGraph**

## Example

`ClrGraph`
