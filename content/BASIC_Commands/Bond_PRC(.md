---
revisions:
- author: YeongJIN COOL
  comment: /\* Bond_PRC(( \*/
  timestamp: '2012-02-16T00:23:54Z'
title: Bond_PRC(
---

# Bond_PRC(

## Description

This command returns in list form bond prices based on specified
conditions.

## Syntax

**Bond_PRC(***MM1*,*DD1*,*YYYY1*,*MM2*,*DD2*,*YYYY2*,*RDV*,*CPN*,*YLD***)**={*PRC*,*INT*,*CST*}

## Example

`Bond_PRC(04,21,1994,01,16,2000,12,12,12)={100,.5.,100}`
