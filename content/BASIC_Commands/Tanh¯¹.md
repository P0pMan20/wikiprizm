---
revisions:
- author: Turiqwalrus
  comment: "Created page with \u2018= Tanh\xAF\xB9 = == Description == This command\n\
    returns the hyperbolic arctangent of the value. == Syntax\n==\u2019\u2018\u2019\
    Tanh\xAF\xB9\u2019\u2019\u2019 \u2018\u2019Value\u2019\u2019 == Example == Tanh\xAF\
    \xB9 .75\u2019"
  timestamp: '2012-02-29T20:26:15Z'
title: "Tanh\xAF\xB9"
---

# Tanh¯¹

## Description

This command returns the hyperbolic arctangent of the value.

## Syntax

**Tanh¯¹** *Value*

## Example

`Tanh¯¹ .75`
