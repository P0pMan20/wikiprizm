---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-24T20:30:40Z'
title: Xmin
---

# XMin

## Description

This command sets the minimum x value displayed in the graphscreen.

## Syntax

*Value*→XMin

## Example

`3→`**`Xmin`**
