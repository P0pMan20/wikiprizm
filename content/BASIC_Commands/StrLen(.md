---
title: StrLen(
---

## StrLen(

## Description

Returns the length of the inputted string

## Syntax

StrLen(\<String name>)

## Example

    "PIE" → Str 1
    Locate 1,1,StrLen(Str 1)       //Displays the length of string 1 at (1,1)
