---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cash_NFV( = == Description == This\ncommand\
    \ returns the net future value. == Syntax\n==\u2019\u2018\u2019Cash_NFV(\u2019\
    \u2019\u2019\u2018\u2019I%\u2019\u2018,\u2019\u2018Cash\u2019\u2019\u2019\u2019\
    \u2018)\u2019\u2019\u2019 == Example ==\nCash_NFV(4.5,3000) \\[\\[Category:BASIC\u2026\
    \u2019"
  timestamp: '2012-02-15T23:50:11Z'
title: Cash_NFV(
---

# Cash_NFV(

## Description

This command returns the net future value.

## Syntax

**Cash_NFV(***I%*,*Cash***)**

## Example

`Cash_NFV(4.5,3000)`
