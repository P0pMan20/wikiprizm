---
revisions:
- author: YeongJIN COOL
  comment: /\* Example \*/
  timestamp: '2012-02-15T20:01:11Z'
title: a1
---

# a1

## Description

This is the a1 value used in recursion. Can be used like the other
variables.

## Syntax

**a1**

## Example

`a1`

`?→a1`
