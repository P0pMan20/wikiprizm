---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Cnvt_APR( = == Description == This\ncommand\
    \ returns the interest rate converted from the effective\ninterest rate to the\
    \ nominal interest rate. == Syntax\n==\u2019\u2018\u2019Cnvt_APR(\u2019\u2019\u2019\
    \u2018\u2019n\u2019\u2018\u2026\u2019"
  timestamp: '2012-02-16T00:19:42Z'
title: Cnvt_APR(
---

# Cnvt_APR(

## Description

This command returns the interest rate converted from the effective
interest rate to the nominal interest rate.

## Syntax

**Cnvt_APR(***n*,*I%***)**

## Example

`Cnvt_APR(10,5.5)`
