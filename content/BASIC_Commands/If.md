---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= If = == Description == This command is\nused\
    \ with If-Then statement along with\u2019\u2018\u2019Then\u2019\u2019\u2018,\u2019\
    \u2018\u2019IfEnd\u2019\u2019\u2018. ==\nSyntax ==\u2019\u2018\u2019If\u2019\u2019\
    \u2019 \u2018\u2019Condition\u2019\u2019 \u2019\u2018\u2019Then\u2019\u2019\u2019\
    \ \u2026Codes\u2026 \u2019\u2018\u2019IfEnd\u2019\u2018\u2026\u2019"
  timestamp: '2012-02-16T23:11:27Z'
title: If
---

# If

## Description

This command is used with If-Then statement along with **Then**,
**IfEnd**.

## Syntax

**If** *Condition*

**Then**

...Codes...

**IfEnd**

## Example

    If A=6
    Then
    A+3
    IfEnd
