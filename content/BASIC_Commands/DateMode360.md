---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= DateMode360 = == Description == This\ncommand\
    \ sets number of days in the year to 360 in financial\ncalculation. == Syntax\
    \ ==\u2019\u2018\u2019DateMode360\u2019\u2019\u2019 == Example == DateMode360\n\
    \\[\\[Cat\u2026\u2019"
  timestamp: '2012-02-15T12:07:30Z'
title: DateMode360
---

# DateMode360

## Description

This command sets number of days in the year to 360 in financial
calculation.

## Syntax

**DateMode360**

## Example

`DateMode360`
