---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Adf = == Description == This is factor A\ndegrees\
    \ of freedom. It is used like a number. ANOVA test must be run\nin order to use\
    \ this. == Syntax ==\u2019\u2018\u2019Adf\u2019\u2019\u2019 == Example == Adf\
    \ \u2026\u2019"
  timestamp: '2012-02-15T23:32:42Z'
title: Adf
---

# Adf

## Description

This is factor A degrees of freedom. It is used like a number. ANOVA
test must be run in order to use this.

## Syntax

**Adf**

## Example

`Adf`
