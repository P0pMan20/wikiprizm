---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= d\xB2/dx\xB2 = == Description == This command\n\
    works just like d/dx(, but this command gets the 2nd derivative of\nthe function.\
    \ == Syntax ==\u2019\u2018\u2019d\xB2/dx\xB2(\u2019\u2019\u2019\u2018\u2019Function\u2019\
    \u2018,\u2019\u2018variable\u2019\u2026\u2019"
  timestamp: '2012-02-15T20:16:33Z'
title: "D\xB2\u2215dx\xB2("
---

# d²/dx²

## Description

This command works just like d/dx(, but this command gets the 2nd
derivative of the function.

## Syntax

**d²/dx²(***Function*,*variable*,*n***)**

## Example

`d²/dx²(3x^2+5,x,5`
