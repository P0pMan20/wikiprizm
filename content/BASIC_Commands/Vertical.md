---
revisions:
- author: YeongJIN COOL
  timestamp: '2012-02-24T20:25:07Z'
title: Vertical
---

# Vertical

## Description

This command draws the vertical line x=given number at graphscreen.

## Syntax

**Vertical** *Value*

## Example

`Vertical 0`
