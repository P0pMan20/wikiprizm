---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= Histograph = == Description == This\nsets the\
    \ stat graph type to Histograph. == Syntax ==\u2019\u2018\u2019Hist\u2019\u2019\
    \u2019 ==\nExample == Hist \\[\\[Category:BASIC_Commands\\]\\]\u2019"
  timestamp: '2012-02-21T00:18:39Z'
title: Hist
---

# Histograph

## Description

This sets the stat graph type to Histograph.

## Syntax

**Hist**

## Example

`Hist`
