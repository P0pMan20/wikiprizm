---
revisions:
- author: YeongJIN COOL
  comment: "Created page with \u2018= d/dx = == Description == This command\nreturns\
    \ the derivative of the specified function respected to\nspecified variable at\
    \ the point at variable=n.\_== Syntax\n==\u2019\u2018\u2019d/dx(\u2019\u2019\u2019\
    \u2018\u2019Fu\u2026\u2019"
  timestamp: '2012-02-15T20:15:33Z'
title: "D\u2215dx("
---

# d/dx

## Description

This command returns the derivative of the specified function respected
to specified variable at the point at variable=n.

## Syntax

**d/dx(***Function*,*variable*,*n***)**

## Example

`d/dx(3x^2+5,x,5`
