---
title: Locale
aliases:
  - /Locale/
---

The Prizm OS supports a few localization features. Much of the UI can be
shown in a language other than English. The OS has six built-in
languages for messages (English, Spanish, German, French, Portuguese and
Chinese), and two built-in languages for the function key labels
(English and Chinese). It is prepared to support a probably unlimited
number of other languages for messages through language add-ins (see
[CASIOWIN]({{< ref "_index.md" >}}) and
[Versions_and_versioning]({{< ref "Versions_and_versioning.md" >}})),
and it probably also supports add-in languages for function key labels.
Function key labels consist of
[bitmaps]({{< ref "FKey_Bitmaps.md" >}}) - there is no text involved,
even for labels which only display text.

The OS uses a special [multi-byte
encoding]({{< ref "Multi-byte_strings.md" >}}) for most non-ASCII
characters. Most fonts can show at least some multi-byte characters, but
not all. See [Fonts]({{< ref "Fonts.md" >}}) for more information.

The user can select a language for messages independently of the
language chosen for function key labels.

At a basic level, translation works by assigning an ID to each string
used in the OS, then translating each string separately. The translation
will appear everywhere the string with that ID is used. Supposing the
string with ID 123 is "Hello World", and a message language translates
ID 123 to "Foo Bar", then every place where string with ID 123 appears,
will show as "Foo Bar".

The settings for number and currency formats appear to be completely
disconnected from the currently selected languages, and there is no way
to change decimal separators and the like. There appears to be no
support for different collation settings (this would be mostly visible
in the way items are sorted in the file browsers). Common words are not
ignored when sorting. Message translation may take into account some
culture aspects, but none of it is automated - things like abbreviations
and numerals must be explicitly specified in the translated messages.

## Locale in add-in software {#locale_in_add_in_software}

Add-ins can display messages and function key labels from the OS library
of messages, but they can't register their own messages or labels. For
messages and labels not included in the OS, or where not all
translations apply (depending on context), add-ins must provide their
own localization system. This is what official add-ins appear to do.

## Add-in languages {#add_in_languages}

Add-in languages are distributed as g3l files, and are meant to be put
in the root of the storage memory, just like add-in software (g3a
files). As far as it is known, each g3l file can only provide one
additional language. These files share the common Casio header. The rest
of the file has the same structure as the message tables included with
the OS.

Once a valid g3l file is installed, its language becomes available for
selecting on the Language menu of the built-in System app. When an
add-in language for messages is selected on that menu, a section of its
g3l file is copied into [Flash]({{< ref "Flash.md" >}}) at address
0x80B60000 / 0xA0B60000, effectively creating a patch that looks very
similar to what can be seen in the areas where messages for built-in
languages are stored. The OS then refers to this memory area when
looking up a certain message. Presumably, the OS keeps somewhere a
pointer to the start of the currently selected message table, and
changing this pointer is what effectively changes the currently selected
message table.

Apparently, if for some reason the OS finds a problem with the installed
add-in language, it will automatically revert the language message
setting to English.

## Using the built-in messages {#using_the_built_in_messages}

Some syscalls will allow you to print localized messages on screen, or
copy them to buffers, given their ID (a positive integer). A list of
such syscalls is available [here]({{< ref "Syscalls/Locale/" >}}). So
far, the IDs for messages have been consistent across OS versions,
something necessary to keep compatibility with existing add-ins.
Messages added with each OS version receive IDs higher than the previous
version's highest ID. However, existing messages may change (because of
e.g. better translation) to something which has the same meaning in the
contexts where Casio has used it so far, but obviously they do not care
about, nor have a way to track, how the messages are used in unofficial
add-ins, so these changes could break your add-ins even if just in
aesthetic terms.

Each message may be up to 88 bytes long. If storing messages on a buffer
(when using
[LocalizeMessage1]({{< ref "Syscalls/Locale/LocalizeMessage1.md" >}}),
for example), make sure your buffer is big enough. For safety, you
should still provide a big enough buffer even if you're sure the message
is much smaller, because one can never be sure of how syscalls decide to
work with the buffers one passes to them. Also note that in the case of
languages like Chinese or Russian, messages take many more bytes than
they appear to take and sometimes many more than their English versions,
partly because of how [multi-byte
encoding]({{< ref "Multi-byte_strings.md" >}}) works.

Messages in Chinese are meant to be printed using a special font that is
wider, reducing the size of the "homescreen" from 21 to 16 rows.
Currently, the English-speaking community of Casio calculator
enthusiasts does not know how to print Chinese characters from add-ins,
even though there is at least one non-official add-in, made by Chinese
enthusiasts, which does it. A serious effort to understand how the OS
and the add-in in question achieves the effect is yet to be made. It
could be that a different set of syscalls would need to be used, but
preliminary reverse-engineering attempts have shown that this doesn't
appear to be the case.

## Seeing the built-in messages {#seeing_the_built_in_messages}

You can see all of the built-in messages and their add-ins, by entering
the [TestMode]({{< ref "Syscalls/TestMode.md" >}}) (after entering,
press 2, 4, then up/down or type a number) or using add-in like [this
one](http://www.cemetech.net/scripts/countdown.php?/prizm/tools/PrintXY_2_Ref.zip&path=archives).
Using the first method, each message is shown in the six built-in
languages at the same time. Using the second method, only the
translation for the currently selected language is shown.

## Using the built-in function key labels {#using_the_built_in_function_key_labels}

Like with built-in messages, the IDs for function key labels have been
consistent across OS versions, with new labels being added at the end of
the tables, with a ID higher than the previous highest. This doesn't
mean existing labels can't change - see the "context" note on the
[#Using the built-in messages](#Using_the_built-in_messages) section.

[GetFKeyPtr]({{< ref "Syscalls/Locale/GetFKeyPtr.md" >}}) will return
the address of the bitmap for a given label ID, taking into account the
currently selected language for function key labels. You can find which
labels are available and their add-ins using Insight's *FKey icons*
option, or the [TestMode]({{< ref "Syscalls/TestMode.md" >}}) (after
entering, press 2 then 0, then use left/right up/down or type an ID). On
the TestMode, English and Chinese versions are shown side by side. When
a Chinese version is the same as the English version, it is shown in
green. Possibly, this means that to save space, duplicates are not
stored, and a pointer to the English bitmap, or some other indicator,
may be used instead.
