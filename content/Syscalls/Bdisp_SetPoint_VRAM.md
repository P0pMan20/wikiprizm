---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T10:53:51Z'
title: Bdisp_SetPoint_VRAM
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0263\
**Function signature:** void Bdisp_SetPoint_VRAM(int x, int y, unsigned short color)

Sets a single point (x, y) in VRAM to the given color.\

## Parameters

-   **x**: pixel column, 0 to 383.
-   **y**: pixel row, 0 to 215.
-   **color**: desired color.\

## Comments

This syscall is exactly like
[Bdisp_SetPoint_DD]({{< ref "Syscalls/Bdisp_SetPoint_DD.md" >}}), except
sets the point in VRAM. It is usually more useful (faster) to write to
VRAM yourself than to use a syscall.
