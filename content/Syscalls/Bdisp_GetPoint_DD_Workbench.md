---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-15T06:12:16Z'
title: Bdisp_GetPoint_DD_Workbench
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x026E\
**Function signature:** unsigned short Bdisp_GetPoint_DD_Workbench(int
x, int y)\

## Comments

This function adds 24 to y and then falls through to
[Bdisp_GetPoint_DD]({{< ref "Syscalls/Bdisp_GetPoint_DD.md" >}}).
