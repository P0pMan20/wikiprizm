---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T17:18:57Z'
title: Restart
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1187\
**Function signature:** void Restart(void)

Immediately restarts the calculator.\

## Comments

After being called, the OS starts as if the button on the back of the
calculator had been pressed, or batteries taken off and put back on. It
is not clear whether certain things like the
[MCS]({{< ref "File_System.md" >}}) are backed up as with a normal power
off (on the "fx-CG10/20 Manager" emulator at least, the MCS is backed
up). So this syscall might not actually provide a true, unexpected
restart, but more of a "nice" one.

The method used for restarting the OS is also unknown: dedicated
circuitry could be used (as with the RESTART button); execution could
jump to the start of the flash, and thus, of the
[bootloader](bootloader) (at 0x8000000), effectively doing the same jump
as the CPU when powered up; or it could jump to the OS entry point (the
same the bootloader jumps to). More complicated and much less likely
approaches could even involve using one of the on-chip RAM areas, such
as IL RAM or RS RAM, as the place to store the code responsible for
restarting, and/or any temporary buffers to be read on start-up, to
trigger special behavior.
