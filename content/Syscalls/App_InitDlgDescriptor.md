---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = App_InitDlgDescriptor\n\\\
    | index = 0x0D79 \\| signature = void App_InitDlgDescriptor(\nvoid\\*P1, unsigned\
    \ char P2 ); \\| header = fxcg/app.h \\| synopsis =\nThe functio\u2026\u201D"
  timestamp: '2014-07-29T10:25:48Z'
title: App_InitDlgDescriptor
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x0D79\
**Function signature:** void App_InitDlgDescriptor( void\*P1, unsigned
char P2 );

The function of this syscall is not yet known in detail, please see
below for more information.\

## Comments

So far, this is what is known about this syscall:

-   Sets \*(unsigned char\*)P1 to P2;
-   Sets \*(unsigned short\*)(P1+2) to 0;
-   Sets 13 bytes starting at \*(unsigned char\*)(P1+4) to 0;
-   Sets \*(unsigned short\*)(P1+0x20) to 0;
-   Sets \*(unsigned int\*)(P1+0x24) to 0;
-   Sets 6 words starting at \*(unsigned short\*)(P1+0x12) to 0xFFFF;
-   Sets \*(unsigned int\*)(P1+0x28) to 0;
-   Sets \*(unsigned char\*)(P1+0x2C) to 0;
-   Sets \*(unsigned char\*)(P1+0x2D) to 0;
-   Leaves some bytes undefined, most probably due to structure
    alignment.
