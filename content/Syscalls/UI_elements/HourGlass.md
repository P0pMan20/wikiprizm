---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:05:39Z'
title: HourGlass
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x02C7\
**Function signature:** void HourGlass(void)\

## Comments

Directly draws the busy-indicator in the upper right corner. The
indicator will automatically spin as this syscall is called more times,
but please note that it doesn't spin every time the syscall is called -
the OS seems to check the RTC to see if enough time has passed for
another spin.

Most [Bfile]({{< ref "Syscalls/Bfile/" >}}) syscalls automatically show
the busy-indicator when operating.
