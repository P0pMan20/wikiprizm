---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:08:11Z'
title: SaveFileDialog
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x0C66\
**Function signature:** int SaveFileDialog(unsigned short\*
filenamebuffer, int mode)

Opens a screen that allows for choosing the location for saving a g3p
file. This is the same screen shown by the Geometry add-in, when saving
a g3p file.\

## Parameters

-   **filenamebuffer** - pointer to buffer for a 16-bit string that will
    hold the filename the user chooses. Must be at least 0x214 bytes
    long.
-   **mode** - meaning not known, should be set to 4.\

## Returns

The meaning of the returned value is unknown.
