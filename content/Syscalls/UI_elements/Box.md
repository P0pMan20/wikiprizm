---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:01:50Z'
title: Box
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x092B\
**Function signature:** void Box(int, int, int, int, int)

The function of this syscall is unknown. It seems to be related to
message boxes.\

## Parameters

The meaning of the parameters is unknown.
