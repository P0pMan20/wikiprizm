---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:04:55Z'
title: FKey_Display
---

## Synopsis

**Header:** display_syscalls.h\
**Syscall index:** 0x0131\
**Function signature:** *void* **FKey_Display**(*int* **n**, *void\**
**p**)

Displays a given [FKey bitmap](FKey_bitmap) above a function key button
(F1 through F6).\

## Parameters

-   *int* **n** - Position of the displayed bitmap. 0 means F1 and 5
    means F6
-   *void\** **p** - Pointer to the bitmap. Use
    [GetFKeyPtr]({{< ref "Syscalls/Locale/GetFKeyPtr.md" >}}) to get
    built-in compatible bitmaps.\

## Example

This code displays "TEST" above F1 button:

    int iresult;
    GetFKeyPtr(0x0003, &iresult);
    FKey_Display(0, iresult);
