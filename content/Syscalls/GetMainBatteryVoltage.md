---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = GetMainBatteryVoltage\n\\\
    | header = fxcg/system.h \\| index = 0x1186 \\| signature = int\nGetMainBatteryVoltage(int\
    \ one) \\| synopsis = Gets the battery\nvoltage. \\| pa\u2026\u201D"
  timestamp: '2014-08-01T16:45:59Z'
title: GetMainBatteryVoltage
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x1186\
**Function signature:** int GetMainBatteryVoltage(int one)

Gets the battery voltage.\

## Parameters

-   **one** - should be set to 1.\

## Returns

The battery voltage in centivolts: if the battery voltage is, for
example, 5.14 V, this syscall will return 514.
