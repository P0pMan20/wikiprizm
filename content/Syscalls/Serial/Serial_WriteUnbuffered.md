---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T10:50:36Z'
title: Serial_WriteUnbuffered
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1BBD\
**Function signature:** int Serial_WriteUnbuffered(unsigned char x)

Transmits a single byte over the serial port, without buffering, by
putting it on the serial transmit FIFO.\

## Parameters

-   **x**: Byte value to transmit.\

## Returns

-   0 for success,
-   1 if the hardware FIFO is not empty,
-   3 if the serial channel is not
    [open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).\

## Comments

To configure the serial port, use
[Serial_Open]({{< ref "Syscalls/Serial/Serial_Open.md" >}}).
[Serial_Write]({{< ref "Syscalls/Serial/Serial_Write.md" >}}) allows you
to queue many bytes for transmission to be sent whenever possible, and
[Serial_WriteSingle]({{< ref "Syscalls/Serial/Serial_WriteSingle.md" >}})
does the same thing as this but with buffering (so it will not fail
unless the transmit buffer is full).
