---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T19:18:48Z'
title: BatteryIcon
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D89\
**Function signature:** void BatteryIcon(unsigned int)

Selects if the battery icon on the left of the status area should be
displayed or not.\

## Parameters

*unsigned int* - set to 0 to hide battery, any other value to show.\

## Comments

Instead of trying to control individual status area items directly,
using [status area
flags]({{< ref "Syscalls/DefineStatusAreaFlags.md" >}}) may be more
appropriate.
