---
revisions:
- author: Gbl08ma
  timestamp: '2016-05-10T19:26:40Z'
title: PrintMiniMini
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x021B\
**Function signature:** void PrintMiniMini(int \*x, int \*y, const
char\* MB_string, int mode, char color, int simulate)

Draws a line of text on the VRAM using the [10 px
font]({{< ref "Fonts.md" >}}), smaller than the ones used by
[PrintMini]({{< ref "Syscalls/PrintMini.md" >}}) and
[Bdisp_MMPrint]({{< ref "Syscalls/Bdisp_MMPrint.md" >}}). It can be seen
on the axis labels of graphs, for example.\

## Parameters

-   *int\** **x** - Pointer to an integer containing the position of the
    text on a horizontal axis, in pixels. It is automatically updated to
    the position for the next character as the text is drawn
-   *int\** **y** - Pointer to an integer containing the position of the
    text on a vertical axis, in pixels. Under normal conditions the
    drawing happens 24 pixels below the coordinate specified, to take
    care of the status area.
-   *unsigned char\** **MB_string** - The string to display. Can include
    a limited set of [multi-byte
    characters]({{< ref "Multi-byte_strings.md" >}}).
-   *int* **mode** - The individual bits of this *int* control different
    aspects:
    -   Bit 0 controls whether **color** should be the back color and
        text color is white;
    -   Bit 2 controls whether the string is written using black as back
        color. The font color is the inverted color.
    -   Bit 4 controls whether the string font used is boldfaced.
    -   Bit 6 controls whether the status area is unlocked (so that the
        y coordinate is not incremented).
    -   Bit 7 controls whether the background is cleared or not.
    -   Other bits have no obvious effect.
-   *char* **color** - indexed color in the range 0 to 7, same as those
    used with [PrintXY]({{< ref "Syscalls/PrintXY.md" >}})
-   *int* **simulate** - if not zero, the function does not actually
    write to VRAM and will only update the x and y values, simulating
    the draw. Useful to check the dimension of a string when drawn.
