---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:17:29Z'
title: Bkey_GetAllFlags
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x0EA0\
**Function signature:** short Bkey_GetAllFlags(void)

Gets flags that control
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}) behavior.\

## Returns

The `short` value at 0xFD80161C.\

## Comments

Known meaning of bits of the short at 0xFD80161C:

-   Bit 0x0001 is involved, when GetKey is running and the OPTN key is
    pressed.
-   Bit 0x0002 is involved, when GetKey is running and the VARS key is
    pressed.
-   Bit 0x0004 is involved, when GetKey is running and the PRGM key is
    pressed.
-   Bit 0x0010 is involved, when GetKey is running and the CLIP key is
    pressed.
-   Bit 0x0020 is checked inside of GetKey.
-   Bit 0x0040 is checked inside of GetKey.
-   Bit 0x0080 is involved, when GetKey is running and the CATALOG key
    is pressed. With this bit set, access to the Function Catalog is
    disabled.
