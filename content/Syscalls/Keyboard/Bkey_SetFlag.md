---
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:17:41Z'
title: Bkey_SetFlag
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x0112\
**Function signature:** void Bkey_SetFlag(short flagpattern)

Sets flags corresponding to keyboard input.\

## Parameters

-   **flagpattern** - all bits corresponding to this parameter will be
    set on the `short` value at 0xFD80161A.\

## Comments

For a description of known flags, see
[Bkey_ClrAllFlags]({{< ref "Syscalls/Keyboard/Bkey_ClrAllFlags.md" >}}).
