---
revisions:
- author: Dr-carlos
  comment: Add link to PGRM_GetKey instead of code block.
  timestamp: '2021-10-12T09:38:37Z'
title: PRGM_GetKey_OS
---

## Synopsis

**Header:** fxcg/keyboard.h\
**Syscall index:** 0x0D39\
**Function signature:** void PRGM_GetKey_OS(unsigned char\* p)

Reads the keyboard input without blocking.\

## Parameters

-   **p** - pointer to 12 bytes `unsigned char` array that will receive
    the keyboard status.\

## Returns

0 if AC/on is pressed, 1 otherwise.\

## Comments

This syscall calls
[GetKeyWait_OS]({{< ref "Syscalls/Keyboard/GetKeyWait_OS.md" >}}) with
KEYWAIT_HALTOFF_TIMEROFF as parameter, hence why it doesn't block.

**p** receives the keyboard status as a BCD-coded value. If no key is
pressed, the BCD value is 0. If the AC/on key is pressed, the BCD value
will have exponent 0xF00 and first nibble of the mantissa 1. Otherwise,
it the BCD value will contain the key matrix-code, with the row number
decremented by 1 to prevent the occurrence of an invalid BCD digit.

To yield the same function result as legacy syscall PRGM_GetKey, one can
use the [PRGM_GetKey]({{< ref "Useful_Routines/PRGM_GetKey.md" >}})
routine included in libfxcg in \<fxcg/keyboard.h\>.
