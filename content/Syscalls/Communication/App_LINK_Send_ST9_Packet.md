---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name =\nApp_LINK_Send_ST9_Packet\
    \ \\| header = fxcg/serial.h \\| index = 0x1398\n\\| signature = int App_LINK_Send_ST9_Packet(void)\
    \ \\| synopsis =\nSends a \u2018\u2019Protocol 7.00\u2019\u2019 p\u2026\u201D"
  timestamp: '2014-08-01T14:31:05Z'
title: App_LINK_Send_ST9_Packet
---

## Synopsis

**Header:** fxcg/serial.h\
**Syscall index:** 0x1398\
**Function signature:** int App_LINK_Send_ST9_Packet(void)

Sends a *Protocol 7.00* packet with subtype 9, not documented in
fxReverse.\

## Returns

During standard communication the return values 0 and 0x14 are allowed.\

## Comments

For more information on *Protocol 7.00*, see
[fxReverse.pdf](http://tny.im/dl/fxReverse2x.pdf).
