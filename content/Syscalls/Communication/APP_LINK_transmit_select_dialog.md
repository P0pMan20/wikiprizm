---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-01T12:18:17Z'
title: APP_LINK_transmit_select_dialog
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1384\
**Function signature:** void APP_LINK_transmit_select_dialog(void\*
buffer1, void\* buffer2);

Invokes the data transmit dialog as shown when F1 is pressed on the Link
menu.\

## Parameters

-   **buffer1** - A pointer to a buffer that is 0x30 bytes long. Should
    be initialized by calling
    [App_InitDlgDescriptor]({{< ref "Syscalls/App_InitDlgDescriptor.md" >}})
    with buffer1 as the first parameter and 0 as the second.
-   **buffer2** - A pointer to a buffer that is 0x214 bytes long. Should
    be initialized with zeros.
