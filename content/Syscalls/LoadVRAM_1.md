---
revisions:
- author: JosJuice
  timestamp: '2015-03-27T15:28:11Z'
title: LoadVRAM_1
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1E63\
**Function signature:** *void* **LoadVRAM_1**(*void*)

Loads the previously saved [VRAM](VRAM) data back from the address
pointed to by
[GetSecondaryVramAddress]({{< ref "Syscalls/GetSecondaryVramAddress.md" >}}).
Use [SaveVRAM_1]({{< ref "Syscalls/SaveVRAM_1.md" >}}) to save the
[VRAM](VRAM).\

## Comments

Since the process of switching from the Main Menu back into the running
app calls this function, you won't be able to restore your VRAM contents
when the Main Menu is opened. If you call this syscall after the Main
process has overwritten the SaveVRAM_1 buffer, nothing will be restored
(there appears to be some intelligence controlling whether the buffer
has been modified since your last call to SaveVRAM_1).
