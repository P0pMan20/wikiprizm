---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_ShapeBase \\|\nindex\
    \ = 0x01C7 \\| signature = void Bdisp_ShapeBase(unsigned char\\*\nwork, struct\
    \ display_shape \\*shape, int color, int line_width, int\nzero1,\u2026\u201D"
  timestamp: '2014-07-29T11:52:31Z'
title: Bdisp_ShapeBase
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x01C7\
**Function signature:** void Bdisp_ShapeBase(unsigned char\* work,
struct display_shape \*shape, int color, int line_width, int zero1, int
zero2)

Draws a shape in VRAM or directly to the display, depending on the bits
set on the first byte of **work**.

## Parameters

-   **work** - pointer to a buffer about which not much is known. If bit
    0 of work\[0\] is set, the shape is drawn on VRAM. If bit 1 of
    work\[0\] is set, the shape is drawn directly on the display (DD).
-   **shape** - a struct specifying the shape.

    ```
    struct display_shape {
        int dx;
        int dy;
        int wx;
        int wy;
        int color;
        void* saved;
    }
    ```

-   **line_width** - thickness of the line used to draw the shape
    outline.
-   **zero1**, **zero2** - meaning unknown, set to zero.
