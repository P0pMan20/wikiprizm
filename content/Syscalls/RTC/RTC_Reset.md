---
revisions:
- author: Gbl08ma
  timestamp: '2015-02-10T23:00:30Z'
title: RTC_Reset
---

## Synopsis

**Header:** fxcg/rtc.h\
**Syscall index:** 0x02BF\
**Function signature:** void RTC_Reset(int mode)

Resets the Real-Time Clock, optionally setting all registers to zero.\

## Parameters

-   **mode** - if not zero, all registers are set to zero.
