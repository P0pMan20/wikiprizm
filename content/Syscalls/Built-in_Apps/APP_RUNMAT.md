---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = APP_SYSTEM \\| index =\n\
    0x1A03 \\| signature = void APP_RUNMAT(int, int); \\| header =\nfxcg/app.h \\\
    | synopsis = Opens the built-in\u201DRun-Mat\u201D app. \\|\nparameters = The\u2026\
    \u201D"
  timestamp: '2014-11-18T22:29:57Z'
title: APP_RUNMAT
---

## Synopsis

**Header:** fxcg/app.h\
**Syscall index:** 0x1A03\
**Function signature:** void APP_RUNMAT(int, int);

Opens the built-in "Run-Mat" app.\

## Parameters

The meaning of the parameters is yet to be known.\

## Comments

Note that even though most built-in apps do not return, the calling code
is kept on stack and one can return to it using a ugly hack, for example
through [timers]({{< ref "/OS_Information/Timers.md" >}}), setjmp and longjmp. The
reason why they don't return is that they expect to use [GetKey as an
exit point]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).
