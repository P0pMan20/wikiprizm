---
bookCollapseSection: true
revisions:
- author: Gbl08ma
  timestamp: '2014-11-18T22:32:23Z'
title: Built-in_Apps
---

These are [syscalls]({{< ref "syscalls.md" >}}) that open OS built-in
apps.

Note that even though most built-in apps do not return, the calling code
is kept on stack and one can return to it using a ugly hack, for example
through [timers]({{< ref "/OS_Information/Timers.md" >}}), setjmp and longjmp. The
reason why they don't return is that they expect to use [GetKey as an
exit point]({{< ref "Syscalls/Keyboard/GetKey.md" >}}).
