---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-31T16:40:48Z'
title: ItoA_10digit
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1633\
**Function signature:** int ItoA_10digit(int, void\*)

The exact function of this syscall is unknown. It appears to be a itoa
variant, with some characteristic related to 10 digits.\

## Parameters

The meaning of the parameters is unknown, but they are likely similar to
the standard itoa ones.\

## Returns

The meaning of the return value is unknown.
