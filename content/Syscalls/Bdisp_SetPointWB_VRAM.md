---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = Bdisp_SetPointWB_VRAM\n\\\
    | index = 0x0262 \\| header = fxcg/display.h \\| signature = void\nBdisp_SetPointWB_VRAM(int\
    \ x, int y, unsigned short color) \\|\nsynopsis\u2026\u201D"
  timestamp: '2014-07-29T10:55:13Z'
title: Bdisp_SetPointWB_VRAM
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x0262\
**Function signature:** void Bdisp_SetPointWB_VRAM(int x, int y, short
unsigned int color)

Sets a single point (x, y) in VRAM to the given color, automatically
adding 24 pixels to the vertical coordinate to skip the status bar.\

## Parameters

-   **x**: pixel column, 0 to 383.
-   **y**: pixel row, 0 to 191.
-   **color**: desired color.\

## Comments

This syscall is exactly like
[Bdisp_SetPoint_VRAM]({{< ref "Syscalls/Bdisp_SetPoint_VRAM.md" >}}),
except for the automatic increment of the vertical coordinate. It is
usually more useful (faster) to write to VRAM yourself than to use a
syscall.
