---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = HexToByte \\| header =\n\
    fxcg/misc.h \\| index = 0x1344 \\| signature = void HexToByte(unsigned\nchar\\\
    * value, unsigned char\\* result) \\| synopsis = Converts the\nhexade\u2026\u201D"
  timestamp: '2014-07-31T17:17:51Z'
title: HexToByte
---

## Synopsis

**Header:** fxcg/misc.h\
**Syscall index:** 0x1344\
**Function signature:** void HexToByte(unsigned char\* value, unsigned
char\* result)

Converts the hexadecimal representation of a byte to a byte.\

## Parameters

-   **value** - hexadecimal representation as a string;
-   **result** - where the resulting byte will go.
