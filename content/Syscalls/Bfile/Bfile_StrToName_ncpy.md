---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T11:41:57Z'
title: Bfile_StrToName_ncpy
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DDC\
**Function signature:** void Bfile_StrToName_ncpy(unsigned short\* dest,
const unsigned char\* source, int n)

Converts a 8 bit character array (array of `unsigned char`) to a 16 bit
string (array of `unsigned short`), suitable for use with [Bfile
syscalls]({{< ref "Syscalls/Bfile/" >}}).\

## Parameters

-   *unsigned short\** **dest** - Pointer to a buffer that will hold the
    resulting 16 bit string.
-   *const unsigned char\** **source** - Pointer to a 8 bit string
    containing the path to convert.
-   *int* **n** - How many bytes to convert (length of the source
    string, see [#Comments](#Comments) for information on the behavior
    of the syscall wen the length doesn't match **n**).\

## Comments

When you use a function, such as Bfile_OpenFile_OS, the path string must
be a 16 bit string. Simply using char16_t works in some cases but not
for all. The strings generated by the two (function vs. 16 bit
characters) are not identical in data.

If the length of **source** is below **n**, **dest** is padded. If
**source** is terminated by 0xFF, **dest** is padded with 0xFFFF,
otherwise it is padded with 0x0000.

[Bfile_NameToStr_ncpy]({{< ref "Syscalls/Bfile/Bfile_NameToStr_ncpy.md" >}})
does the opposite of this syscall.
