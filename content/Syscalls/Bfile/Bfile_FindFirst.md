---
revisions:
- author: Ahelper
  comment: Added the address description
  timestamp: '2014-11-28T16:45:19Z'
title: Bfile_FindFirst
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DB7\
**Function signature:** int Bfile_FindFirst(const unsigned short\*
pathname, int\* FindHandle, const unsigned short\* foundfile,
file_type_t\* fileinfo)

This function lets you initialize a search in the storage memory for a
list of files and folders and retrieves info about each. To read all
files and folders, use
[Bfile_FindNext]({{< ref "Syscalls/Bfile/Bfile_FindNext.md" >}}) after
calling this.\

## Parameters

-   *const unsigned short\** **pathname** - The 16 bit string containing
    the path of which to search (you may use one '\*' as a wildcard, ex:
    "\\\\fls0\\\*.g3a"). You must prefix \\\\fls0\\ to the path.\
-   *int\** **FindHandle** - Search handle. This *must* be closed using
    [Bfile_FindClose]({{< ref "Syscalls/Bfile/Bfile_FindClose.md" >}})
    when you're done finding files.\
-   *const unsigned short\** **foundfile** - Will contain the file or
    folder name as a 16 bit string. Doesn't contain the full path.\
-   *file_type_t* **fileinfo** - Pointer to a *file_info_t* struct. See
    comments for definition.\

## Returns

-   0 on success
-   -1 if *pathname* is invalid
-   -5 if "\\\\fls0\\\\" is not prefixed to *pathname*
-   -16 if no files were found. This is an expected value, not a
    critical failure. This means that **FileHandle** should be closed
    with
    [Bfile_FindClose]({{< ref "Syscalls/Bfile/Bfile_FindClose.md" >}}).\

## Comments

Please remember to close **FindHandle** using
[Bfile_FindClose]({{< ref "Syscalls/Bfile/Bfile_FindClose.md" >}}).

Here is the file_type_t definition:

    typedef struct
    {
      unsigned short id, type;
      unsigned long fsize, dsize;
      unsigned int property;
      unsigned long address;
    } file_type_t;

The **address** property contains the absolute position of the file's
data in the flash chip in the P2 memory area (0xAnnnnnnn).

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.\

## Example

See [the Using the File System
tutorial]({{< ref "Tutorials/Using_the_File_System.md" >}}) for an
example
