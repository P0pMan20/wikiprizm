---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T10:40:45Z'
title: Bfile_FindNext_NON_SMEM
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1DB8\
**Function signature:** int Bfile_FindNext_NON_SMEM(int FindHandle,
const unsigned short\* foundfile, file_type_t\* fileinfo)

This function is used to list files and folders, on a device other than
the storage memory, based on a find handle created by
[Bfile_FindFirst_NON_SMEM]({{< ref "Syscalls/Bfile/Bfile_FindFirst_NON_SMEM.md" >}}).
Probably has no practical application on the Prizm, since there is no SD
card slot and the main memory doesn't count as a device (use the
[MCS]({{< ref "Syscalls/MCS/" >}}) syscalls instead).\

## Parameters

-   *int* **FindHandle** - The find handle created by
    [Bfile_FindFirst_NON_SMEM]({{< ref "Syscalls/Bfile/Bfile_FindFirst_NON_SMEM.md" >}}).
-   *const unsigned short\** **foundfile** - The string used to store
    the found file or folder name.
-   *file_type_t\** **fileinfo** - Pointer to a *file_info_t* struct.
    The members of this struct are not used as documented on
    [Bfile_FindFirst]({{< ref "Syscalls/Bfile/Bfile_FindFirst.md" >}})!\

## Returns

0 on success, or a negative error code on failure.\

## Comments

This syscall is probably useful on a system with SD card slot, for
example a future model of the Prizm. However, there should be no need to
use it directly, as syscall
[Bfile_FindNext]({{< ref "Syscalls/Bfile/Bfile_FindNext.md" >}}) will
automatically call this one when a device other than the storage memory
is used.

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) functions while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with functions that change data
on the file system. Make sure to stop and uninstall all timers before
using Bfile functions (and optionally restore them later). See
[Incompatibility_between_Bfile_Syscalls_and_Timers]({{< ref "Incompatibility_between_Bfile_Syscalls_and_Timers.md" >}})
for more information.
