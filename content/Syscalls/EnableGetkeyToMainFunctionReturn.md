---
title: EnableGetkeyToMainFunctionReturn
---

## Synopsis

**Header:** fxcg/keyboard.h
**Syscall index:** 0x1EA6
**Function signature**: `void EnableGetkeyToMainFunctionReturn(void)`

Sets the current behavior of [GetKey]({{< ref "Keyboard/GetKey.md" >}}) so that
pressing MENU will return to the Main Menu.

## Comments

[GetGetkeyToMainFunctionReturnFlag]({{< ref GetGetkeyToMainFunctionReturnFlag.md >}})
can be used to determine whether the MENU will return to the Main Menu, and
[DisableGetkeyToMainFunctionReturnFlag]({{< ref DisableGetkeyToMainFunctionReturn.md >}})
can be used to disable this.
