---
revisions:
- author: ProgrammerNerd
  timestamp: '2015-02-15T01:58:57Z'
title: SetSetupSetting
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x0032\
**Function signature:** void SetSetupSetting(unsigned int
SystemParameterNo, unsigned char SystemParameterValue)

Sets a [Setup]({{< ref "Setup.md" >}}) setting to the given value.\

## Parameters

-   **SystemParameterNo** - index of the setting to change.
-   **SystemParameterValue** - new value of the setting.\

## Comments

This function does no bounds checking meaning that with an incorrect
value of SystemParameterNo you may accidentally overwrite a byte that
does not pertain to setup settings.\

## Example

The following example sets the keyboard input method to lowercase
alpha-lock:

`SetSetupSetting((unsigned int)0x14, 0x88);`
