---
revisions:
- author: Gbl08ma
  timestamp: '2014-08-03T22:52:56Z'
title: CLIP_Store
---

## Synopsis

**Header:** fxcg/system.h\
**Syscall index:** 0x07E5\
**Function signature:** int CLIP_Store(unsigned char\* buffer, int
length)

Stores a buffer to the system clipboard, that users can paste later by
pressing PASTE (Shift+9).\

## Parameters

-   **buffer** - pointer to buffer to store.
-   **length** - size of the content to store.\

## Returns

The meaning of the return value is yet to be investigated.\

## Comments

This syscall writes the buffer to the
[MCS]({{< ref "File_System.md" >}}) item **CLIP** located in the
**\@REV2** folder (one can inspect the true, unfiltered contents of the
MCS using an add-in like Insight or the
[TestMode]({{< ref "Syscalls/TestMode.md" >}})).
