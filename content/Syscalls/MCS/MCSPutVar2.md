---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T14:26:24Z'
title: MCSPutVar2
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x154D\
**Function signature:** int MCSPutVar2(unsigned char\* dir, unsigned
char\* item, int data_len, void\* buffer)

Creates main memory **item** in **dir** with **data_len** bytes of data
from **buffer**.\

## Parameters

-   *unsigned char\** **dir** - The name of the directory where the item
    will be located.
-   *unsigned char\** **item** - The name of the item to create.
-   *int* **data_len** - The length of data to write.
-   *void\** **buffer** - The data to write.\

## Returns

-   0 for success;
-   37 when the item already exists;
-   other values on other kinds of failure.
