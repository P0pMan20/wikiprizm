---
revisions:
- author: Gbl08ma
  comment: "Created page with \u201C{{Syscall \\| name = MCS_GetState \\| header\n\
    = fxcg/file.h \\| index = 0x1529 \\| signature = int\nMCS_GetState(int\\* maxspace,\
    \ int\\* currentload, int\\*\nremainingspace) \\| synopsis = Gets\u2026\u201D"
  timestamp: '2014-07-30T14:50:22Z'
title: MCS_GetState
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1529\
**Function signature:** int MCS_GetState(int\* maxspace, int\*
currentload, int\* remainingspace)

Gets information about the total, taken and available space of the Main
Memory.\

## Parameters

-   **maxspace** - Pointer to *int* that will receive the amount of
    total MCS space;
-   **currentload** - Pointer to *int* that will receive the amount of
    used MCS space;
-   **remainingspace** - Pointer to *int* that will receive the amount
    of unused MCS space.\

## Returns

Always returns 0.\

## Comments

One may think that **maxspace** is constant, which is not true, because
when running as an eActivity strip, add-ins have access to a smaller MCS
that's contained within the eActivity file and shrinks as more data is
added to it.
