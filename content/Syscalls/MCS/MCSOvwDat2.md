---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-30T14:23:10Z'
title: MCSOvwDat2
---

## Synopsis

**Header:** fxcg/file.h\
**Syscall index:** 0x1552\
**Function signature:** int MCSOvwDat2(unsigned char\* dir, unsigned
char\* item, int bytes_to_write, void\* buffer, int write_offset)

Overwrites data in the main memory item **item** located in directory
**dir**, starting at **write_offset** with **bytes_to_write** bytes of
data from **buffer**.\

## Parameters

-   *unsigned char\** **dir** - The name of the directory where the item
    is located.
-   *unsigned char\** **item** - The name of the item.
-   *int* **bytes_to_write** - The length of data to write.
-   *void\** **buffer** - The data to write.
-   *int* **write_offset** - Where in **item** to start writing.\

## Returns

-   0 for success;
-   other values on failure.
