---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T19:28:26Z'
title: RealIcon
---

## Synopsis

**Header:** fxcg/display.h\
**Syscall index:** 0x1D8C\
**Function signature:** void RealIcon(unsigned int)

The exact function of this syscall is unknown; it is probably related to
status area items.\

## Parameters

The meaning of the parameter is unknown.\

## Comments

Instead of trying to control individual status area items directly,
using [status area
flags]({{< ref "Syscalls/DefineStatusAreaFlags.md" >}}) may be more
appropriate.
