---
title: Compare/Match Timer
aliases:
  - /Compare_Match_Timer/
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

The SH7305 CPU used on the Prizm contains, among other
[peripherals]({{< ref "peripherals.md" >}}), a Compare Match Timer
(CMT), whose base address is 0xA44A0000.

Some [syscalls exist for interfacing with the
CMT]({{< ref "Syscalls/CMT/" >}}).
