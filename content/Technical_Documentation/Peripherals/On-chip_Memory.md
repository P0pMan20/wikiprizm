---
title: On-chip Memory
aliases:
  - /On-chip_Memory/
---

The SH7305 is equipped with a number of different built-in memory areas with
various properties.

## RS Memory {#rs_memory}

-   Size: 16376 bytes
-   Address range: 0xFD800000 to 0xFD803FF8

Holds important OS values and code (including code that writes to the
[Flash]({{< ref "Flash.md" >}})), should not be modified carelessly.

## IL Memory {#il_memory}

-   Size: 4096 bytes
-   Address range: 0xE5200000 to 0xE5200FFF

Appears to not be used by the OS.

## X Memory {#x_memory}

-   Size: 8192 bytes
-   Address range: 0xE5007000 to 0xE5008FFF

Divided in two pages: page 0, from 0xE5007000 to 0xE5007FFF, and
page 1, from 0xE5008000 to 0xE5008FFF.

Memory from 0xE5007000 to 0xE500701F has memory written to it then read
back in the bootloader.

## Y Memory {#y_memory}

-   Size: 8192 bytes
-   Address range: 0xE5017000 to 0xE5018FFF

Divided in two pages: page 0, from 0xE5017000 to 0xE5017FFF, and
page 1, from 0xE5018000 to 0xE5018FFF.

Appears to not be used by the OS.
