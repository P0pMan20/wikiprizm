---
revisions:
- author: Dr-carlos
  comment: Add note about fx-CG50 Location
  timestamp: '2022-03-24T10:01:45Z'
title: Product_ID_Information
---

Every Casio Prizm contains a small white sticker on the back, below the
RESTART button and above the battery compartment, with a Code 128
barcode and its content. This may also be next to be batteries,
underneath the battery case on the fx-CG50. This is the product ID and
contains, among other data, the calculator production year and month.

Here's how to read the product ID:

`mmmmmYMv`**`A`**`xxxxxx`

-   mmmmm - model. 755AW for the fx-CG 10, 755DW for the fx-CG 20, 755CW
    for the blue "School Property" version of the fx-CG 10[^school-property],
    810AW for the fx-CG 50, 810FW for the fx-CG 50 AU, and 811AW for the Graph
    90+E.
-   Y - last digit of production year, starting in 2008 and ending in
    2017.
-   M - production month. 1 through 9 for January-September, X for
    October, Y for November and Z for December.
-   v - version. Seems to be "M" for the fx-CG 10 and fx-CG 20, and "Q" for the fx-CG 50 and Graph 90+E.
-   **A** - appears to always be "A" on the Prizm.
-   xxxxxx - serial number.

[^school-property]: Some photos of this model can be found [here](http://shaiwu.smzdm.com/p/29827).

**Examples:**

Given the ID

`755AW0XMA004579`

...we can extract the following information:

-   The calculator is a fx-CG 10 (755AW);
-   It was produced in October 2010;
-   It was the 4579th Prizm calculator to be produced.

Given the ID

`755DW14MA067642`

...we can extract the following information:

-   The calculator is a fx-CG 20 (755DW);
-   It was produced in April 2011;
-   It was the 67642nd Prizm calculator to be produced.

Given the ID

`810FW79QA077454`

...we can extract the following information:

-   The calculator is a fx-CG 50 AU (810FW);
-   It was produced in September 2017;
-   It was the 77454th Prizm calculator to be produced.

## CASIOABS ID {#casioabs_id}

Every Casio Prizm contains a 8-byte alphanumeric ID in the first boot
sector, more precisely at the address 0x8001FFD0, where
[CASIOABS]({{< ref "CASIOABS.md" >}}) is located. This ID is random and
appears to be included so that the device can conform to the USB spec
(according to which every device must expose a unique ID). When
connected through USB, the calculator indeed uses this ID.

## Footnotes

