---
revisions:
- author: Ahelper
  comment: Updating
  timestamp: '2012-09-11T01:03:13Z'
title: Hello_World
---

It's time that you get a feel for how addins are created for the Casio
Prizm. Addins are not created the same way that programs are made for
PCs or other devices.

## Creating the Project {#creating_the_project}

The easiest way to create projects using the PrizmSDK is to copy an
existing project directory. If this is indeed your first project with
the PrizmSDK, then you should already have a project folder named
'example' in the *projects* folder. Make a copy of this folder and name
it something simple (such as tut1).

This folder houses all source code, resources, and the build system for
your addin. The first thing that you need to do before starting on your
project is to set up the build system. Open up the *Makefile* file. You
will see that it is already set up to compile all sources in the *src*
folder and make an addin. Find the line starting with

`MKG3AFLAGS := ...`

This line is important when you start a new project. This line
determines the name of your addin and what icons it uses (will be
touched on later). Please see the [mkg3a usage
section]({{< ref "mkg3a.md" >}}) for the an explanation for the
parameters. For now, you need to modify the name of the addin. Change
the *-n basic:example* and change the *example* with the name of your
addin.

## Test Building {#test_building}

Now, you have the build system set up. In order to test, we need to add
some code to your *src* folder. First, delete any files already in your
*src* folder and make a new .c or .cpp file. I will use the name
*main.cpp* for the source file. Open up your source file and put the
following into it:

    int main()
    {
      return 0;
    }

This doesn't do anything and shouldn't be run on the Prizm, but we need
something to test building of your addin. In a terminal (Unix) or
cmd.exe (Windows), cd into the project folder and run make. For example,
if your project is named *tut1*, you would do

    $ cd [PrizmSDK Path]/projects/tut1
    $ make

*The above example is for unix, Windows users should modify the above
commands*\
If all goes well, you should have no build errors and will have a .g3a
in your tut1 folder. If you have problems, you should post on the
[Cemetech Prizm subforum](http://www.cemetech.net/forum/viewforum.php?f=68).
This is basically the method of producing a .g3a.

## Making Your First Addin {#making_your_first_addin}

Now that you have a project folder setup up, you can learn the ropes on
how addins are coded. First, you should understand certain things before
trying to code:

-   The Prizm doesn't have libc or libstdc++ built in. This means that
    you cannot use functions available when programming on other
    platforms, like printf, file IO, std namespace, etc.. **Edit:**
    PrizmSDK 0.4 will have these features!
-   The Prizm has its own functions that aren't completely documented.
    This means that you have to either read other addins' source code,
    look for community documentation, or by using this wiki. Please help
    us continue to expand this wiki!
-   The main function shouldn't return. The addin cannot close itself,
    you have to have the OS close it from user key input. If you do
    return inside of the main function, your addin cannot be resumed
    again until the OS closes it.
-   Headers are still being reorganized and functions may be missing or
    in odd headers. (See below)
-   The current PrizmSDK (0.3) is outdated and has incorrect functions
    and headers. The best option is to grab libfxcg directly from
    [github](https://github.com/Jonimoose/libfxcg).

Now then, lets start adding in your code to print Hello World to the
screen.

    #include <fxcg/display.h>
    #include <fxcg/keyboard.h>

As I noted above, headers are a bit spread out. These headers include a
few important functions, such as
[PrintXY]({{< ref "Syscalls/PrintXY.md" >}}) and
[GetKey]({{< ref "Syscalls/Keyboard/GetKey.md" >}}). Now, we can start
by adding in a normal main function

    int main() {
      return 0; // Needed if you have main returning int, but will never be reached.
    }

Now, if you read the documentation of
[PrintXY]({{< ref "Syscalls/PrintXY.md" >}}), you will see that you can
print out colored text in letter coordinates. You should also note that
the string must start with 2 garbage letters. It is thought that the
string starts with an actual 16 bit number then the string. Also,
unexpectedly, X and Y start at 1, not 0. We can add in a simple example
to your main function, such as

`PrintXY(3,8,"--Hello World!", TEXT_MODE_NORMAL, TEXT_COLOR_RED); // Place it somewhere on the screen`

TEXT\_\* are defined in color.h. You can look in the header to see your
options.

Now, because running this code will hit a return after printing text, we
need a better way to have the addin run. The GeyKey function will wait
for a key to be pressed and will handle the \[MENU\] key, so users can
exit your addin. This is done by the OS and the \[MENU\] key isn't known
to your addin. Lets add in the code to read the key forever so users can
quit your addin:

    int key;
    while(1)
      GetKey(&key); // Loop forever

This is all of the code needed to run your addin! You can build and run
your addin.

### Finished Code {#finished_code}

This is the resulting code from this tutorial:

    #include <fxcg/display.h>
    #include <fxcg/keyboard.h>
    int main() {
      PrintXY(3,8,"--Hello World!", TEXT_MODE_NORMAL, TEXT_COLOR_RED); // Place it somewhere on the screen.  Note that x,y are 1-indexed.
      int key;
      while(1)
        GetKey(&key); // Loop forever
      
      return 0; // Needed if you have main returning int, but will never be reached.
    }

## Putting Addins on Your Prizm {#putting_addins_on_your_prizm}

.g3a files are added to your Prizm by simply copying them to the root
folder of your Prizm. Connect your Prizm to your PC, copy the .g3a to
the drive (not in a folder), then eject the drive. Your addin should
show up in the Main Menu of your Prizm.
