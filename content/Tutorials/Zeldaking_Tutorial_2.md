---
revisions:
- author: Ahelper
  comment: Organization
  timestamp: '2012-07-30T19:39:18Z'
title: Zeldaking_Tutorial_2
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

In this section of the tutorial, you’ll learn the development of icons,
become more comfortable with the SDK, and learn the layout of a C
program coded for the Prizm. Various tasks you’ll be able to complete at
the end of reading this section include:

-   Correct Icon development
-   Correctly compile C programs using the SDK
-   Identify the general layout of a C program coded for the Prizm

## Icon Development {#icon_development}

Go [here](http://cemetech.net/forum/viewtopic.php?t=6211&start=0).
(credits go to JosJuice)

## Hello World -- Familiarizing Yourself With the Compiling Process {#hello_world____familiarizing_yourself_with_the_compiling_process}

Since you should have the SDK set up and have the correct files where
need be (if not read Section 1) we can continue. Open up Notpad++ and
copy this into it.

    #include <keyboard_syscalls.h> 
    #include <keyboard.hpp> 
    #include <display_syscalls.h> 
    #include <color.h> 
    #define true 1 
    int main() { 
    PrintXY(1, 1, "XXHello World!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK); 
    int key; 
    while(true) { 
    GetKey(&key); 
    Bdisp_PutDisp_DD(); 
    } 
    return 0; 
    } 

Save this file as a C source file named, "Hello_World". Click into the
Prizm SDK and navigate into the projects folder. Copy and paste another
folder of the "example" folder. Rename one of them to "Hello_World". You
will need to do this for each project, always copying the example folder
(this folder holds the basic files needed for each project). Now you
will need to find where the "Hello_World" C source file is saved and
move it into the new "Hello_World" folder.

Note: make sure your project folder name does not have spaces in it,
either in the folder name or in the makefile; using spaces will cause
the build system to not recognize your project.

Select your source and copy it into the “src” folder. Assuming you have
your two icons made, we can continue. Rename your icons, “selected” and
“unselected”. Next right click on the “Makefile” (this is the file not
the Batch), and select “Edit it with Notepad++”. Scroll down until you
will see this:

`TARGET  := $(notdir $(CURDIR)) `

Where it says “\$(notdir \$(CURDIR))” replace it with “Hello_World” (or
whatever your project name is) Now scroll down again until you see this:

`MKG3AFLAGS := -n basic:example -i uns:../unselected.bmp -i sel:../selected.bmp`

Where it says “basic:example” replace the “example” with “Hello World”,
you will rename this to whatever you want your add-in to be named. Now
time for compiling, run the make.bat (the batch file) and a command
prompt screen should appear and compile your code. After compiling the
.g3a file will be in your project folder. Connect your Prizm and drag it
into the main memory folder.

## The General C Format for the Prizm {#the_general_c_format_for_the_prizm}

Because you are programming C for the Prizm, you need to follow this
general format that stays consistent. To be consistent, the program will
continue until you press \[menu\] which returns you to the main menu.

    //Headers go here 
    #include <keyboard_syscalls.h>
    //defines go here 
    #define true 1   //declares true to equal 1 
    //global variable declarations go here 
    int main () { 
       //local variable declarations go here 
    int key; 
    while(true) { 
    GetKey(&key); 
    //code goes here 
       } 
    return 0; 
    } 
    //functions go here 

Along with following this format, indent each block of code (Notepad++
does this for you). This increases readability and allows others to
understand your code.

## Credits

Zeldaking

Ashbad
