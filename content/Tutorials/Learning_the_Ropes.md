---
revisions:
- author: Dr-carlos
  comment: Rename common to toolchain
  timestamp: '2021-10-07T20:22:47Z'
title: Learning_the_Ropes
---

*This page has not been completed. Parts may be missing or reorganized
before completed. Information is provided as-is and may have errors.*

This tutorial will introduce you to the Prizm SDK (version 0.3), how it
is structured, and how you create and build projects.

## The Include Folder {#the_include_folder}

This folder contains required headers for C/C++ programs. It is known
that the headers are a bit chaotic and a rebuild of the headers is being
done.

## The Toolchain Folder {#the_toolchain_folder}

The toolchain folder is home of two files, prizm.ld and prizm_rules.
*prizm.ld* is a file that is used by the linker when compiling to
provide information about the prizm, how data and ram is laid out, and
where to put stuff. *prizm_rules* is a Makefile that is included by
Makefiles in the project folder. This file defines common settings and
is needed for building. This folder used to be named 'common'.

## The Project Folder {#the_project_folder}

This folder is set up to simplify storing and building your Prizm
projects.

### Makefiles

If you do not know what these are, please [read the GNU Make
page](http://www.gnu.org/software/make/). The Makefiles used in each
subproject are pre-made and require little modification. The file's
purpose is simply to build all source files under src/ and make a .g3a.

### mkg3a and g3a's {#mkg3a_and_g3as}

[mkg3a]({{< ref "mkg3a.md" >}}) is a utility to convert a binary, along
with the selected and deselected icons, into a .g3a, executable on the
Prizm.

## Making projects {#making_projects}
