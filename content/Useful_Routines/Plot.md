---
revisions:
- author: Dr-carlos
  comment: Use GetVRAMAddress not 0xA8000000
  timestamp: '2022-03-17T10:44:25Z'
title: Plot
---

## Synopsis

Plots a point at (x,y) on the screen with a specified color

## Source code {#source_code}

    #include <fxcg/display.h>
    void plot(unsigned x,unsigned y,unsigned short color){
        unsigned short*s=(unsigned short*) GetVRAMAddress;
        s+=(y*384)+x;
        *s=color;
    }

## Inputs

x - Specifies the x coordinate of the pixel in range of \[0,383\]\
y - Specifies the y coordinate of the pixel in range of \[0,215\]\
color - Specifies what color the plotted pixel will be. It is in RGB 565
format.

## Notes

Given the minimal nature of this function, call overhead can easily be a
non-trivial factor in obtaining reasonable performance. Marking it as
`static inline` and declaring it in a single header file may be
[appropriate]({{< ref "Optimization_Tips.md" >}}) to help ensure this
function is inlined.

## Comments

Function written by ProgrammerNerd
