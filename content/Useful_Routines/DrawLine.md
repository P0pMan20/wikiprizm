---
revisions:
- author: ProgrammerNerd
  comment: /\* Comments \*/
  timestamp: '2014-04-18T02:03:48Z'
title: DrawLine
---

## Synopsis

Uses the Bresenham line algorithm to draw a line starting at (x1,y1)
ending at (x2,y2) with a specified RGB 565 color.

## Source code {#source_code}

    void drawLine(int x1, int y1, int x2, int y2, unsigned short color) {
        signed char ix;
        signed char iy;
     
        // if x1 == x2 or y1 == y2, then it does not matter what we set here
        int delta_x = (x2 > x1?(ix = 1, x2 - x1):(ix = -1, x1 - x2)) << 1;
        int delta_y = (y2 > y1?(iy = 1, y2 - y1):(iy = -1, y1 - y2)) << 1;
     
       plot(x1, y1, color);  
        if (delta_x >= delta_y) {
            int error = delta_y - (delta_x >> 1);        // error may go below zero
            while (x1 != x2) {
                if (error >= 0) {
                    if (error || (ix > 0)) {
                        y1 += iy;
                        error -= delta_x;
                    }                           // else do nothing
             }                              // else do nothing
                x1 += ix;
                error += delta_y;
                plot(x1, y1, color);
            }
        } else {
            int error = delta_x - (delta_y >> 1);      // error may go below zero
            while (y1 != y2) {
                if (error >= 0) {
                    if (error || (iy > 0)) {
                        x1 += ix;
                        error -= delta_y;
                    }                           // else do nothing
                }                              // else do nothing
                y1 += iy;
                error += delta_x;  
                plot(x1, y1, color);
            }
        }
    }

## Inputs

x1 - X coordinate of the start of the line.\
y1 - Y coordinate of the start of the line.\
x2 - X coordinate of the end of the line.\
y2 - Y coordinate of the end of the line.\
color - The color of the line.

## Notes

This function requires the [plot]({{< ref "Useful_Routines/plot.md" >}})
function.

## Comments

Function was originally written by Christopher "Kerm Martian" Mitchell.
