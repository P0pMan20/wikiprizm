---
revisions:
- author: Dr-carlos
  comment: Use GetVRAMAddress() not 0xA8000000
  timestamp: '2022-03-17T10:42:49Z'
title: CopySpriteMaskedAlpha
---

## Synopsis

Draws a sprite with a mask color, with an additional alpha mask, which
is consistent throughout the whole rendering of the sprite. Draws to
Video RAM (VRAM).

### Declaration

```
#include <fxcg/display.h>

void CopySpriteMaskedAlpha(const void*datar, int x, int y, int width, int height, color_t maskcolor, int alpha) { 
   color_t*data = (color_t*) datar; 
   color_t* VRAM = (color_t*)GetVRAMAddress(); 
   VRAM += LCD_WIDTH_PX*y + x; 
   alpha %= 32; 
   for(int j=y; j<y+height; j++) { 
      for(int i=x; i<x+width;  i++) { 
         if (*(data) != maskcolor) { 
         *(VRAM) = (color_t)((((int)(*data & 0xf81f) * alpha + (int)(*VRAM & 0xf81f) * (32-alpha) + 0x8010) >> 5) & 0xf81f) | 
                (color_t)((((int)(*data & 0x07e0) * alpha + (int)(*VRAM & 0x07e0) * (32-alpha) + 0x0400) >> 6) & 0x07e0); 
           VRAM++; data++; 
         } else { VRAM++; data++; } 
      } 
      VRAM += LCD_WIDTH_PX-width; 
   } 
}
```

### Inputs

*const void\*datar*: pointer to sprite data

*int x, int y*: Video RAM position to draw to

*int width, int height*: width and height of the sprite

*color_t maskcolor*: color not to be rendered (masked off)

*int alpha*: amount of alpha transparency to be applied, from 0-32; 32
is opaque, 0 is entirely transparent.

### Outputs

None

## Comments

The sprite drawn is not clipped at the edges of Video RAM.
