---
revisions:
- author: Gbl08ma
  timestamp: '2014-07-29T23:32:43Z'
title: Incompatibility_between_Bfile_Syscalls_and_Timers
---

Using [Bfile]({{< ref "Syscalls/Bfile/" >}}) syscalls while user
[timers]({{< ref "/OS_Information/Timers.md" >}}) are installed can cause SYSTEM ERRORs
and other undefined behavior, especially with syscalls that change data
on the [file system]({{< ref "File_System.md" >}}). This is because the
Bfile functions perform storage memory optimization whenever it is
needed. Storage memory optimization - the same operation available from
the Memory menu, except that here it's done in smaller steps - shuffles
data around in the [flash]({{< ref "flash.md" >}}), meaning that the
add-in code location often changes. This is usually not a problem,
because once optimization finishes, the virtual address of the add-in
ROM is remapped to the new add-in location and execution continues
without issue. The problem lies on the time elapsed between the start of
optimization and the updating of memory maps.

If there are timers running, they will keep running as optimization is
performed. During the aforementioned period of time, the add-in ROM
(and, thus, the handlers) will be mapped to something that isn't the
add-in code anymore, meaning that if a timer handler is called,
execution will jump to whatever is there at the time. This usually
causes a hang, reboot or SYSTEM ERROR, but in the rare case that there
is meaningful code at the address (for example, another add-in), it will
get executed, possibly causing data corruption or even a full device
brick (if the random code happens to erase the first flash sector, where
the one and only [bootloader](bootloader),
[CASIOABS]({{< ref "CASIOABS.md" >}}), is located).

To avoid these problems, always stop and uninstall any user timers
before calling [Bfile]({{< ref "Syscalls/Bfile/" >}}) syscalls and, if
necessary, don't forget to install and start the timers you uninstalled,
when you're done with Bfile.

Putting timer handlers in a section of RAM (or even flash) that is not
touched during the Bfile operations, may also be a valid approach to
solving the problem, if timers must absolutely be kept running during
the Bfile operations.
